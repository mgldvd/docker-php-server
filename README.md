# Docker php server

<p float="left">
  <img src="https://cdn.svgporn.com/logos/docker.svg" height="40" title="docker">
  <img src="https://www.svgrepo.com/show/304556/three-dots.svg" height="30" title="dots">
  <img src="https://cdn.svgporn.com/logos/php.svg" height="40" title="php">
  <img src="https://www.svgrepo.com/show/304556/three-dots.svg" height="30" title="dots">
  <img src="https://cdn.worldvectorlogo.com/logos/apache-13.svg" height="40" title="apache">
</p>

Docker + PHP 8.2 + Apache

## 📍 Start

```bash
task up

# or

docker compose up -d
```

## 📍 Make change on `Dockerfile`

```bash
task build
```

## Taksfile Install (command)

```bash
sh -c "$(curl --location https://taskfile.dev/install.sh)" -- -d
```

## DOCS:
- https://taskfile.dev/